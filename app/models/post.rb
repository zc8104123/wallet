class Post < ActiveRecord::Base
	belongs_to :user
	has_many :comments, :dependent => :destroy
	has_many :talks, :dependent => :destroy
	has_many :costs, :dependent => :destroy
	has_many :incomes, :dependent => :destroy
end
