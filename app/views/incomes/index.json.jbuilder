json.array!(@incomes) do |income|
  json.extract! income, :id, :post_id, :body, :money
  json.url income_url(income, format: :json)
end
