json.array!(@costs) do |cost|
  json.extract! cost, :id, :post_id, :body, :money
  json.url cost_url(cost, format: :json)
end
