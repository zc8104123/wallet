json.array!(@talks) do |talk|
  json.extract! talk, :id, :post_id, :body
  json.url talk_url(talk, format: :json)
end
