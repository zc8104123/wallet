require 'test_helper'
class PostTest < ActiveSupport::TestCase
	test "invalid_with_empty_attributes" do
		post = Post.new 
		assert post.invalid?  
		assert post.errors[:title].any? 
		assert post.errors[:body].any?  
	end
	test "requires_body" do
		post = Post.create(:body => "This is an awesome post.")
		assert post.invalid?
		assert post.errors[:title].any?
	end
	test "requires_title" do
		post = Post.create(:title => "My Great Post")
		assert post.invalid?
		assert post.errors[:body].any?
	end
	test "valid_with_title_and_body" do
		post = Post.create(:title => "My Great Post", :body => "This is an awesome post.")
		assert post.valid?
	end
end
