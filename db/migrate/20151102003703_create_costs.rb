class CreateCosts < ActiveRecord::Migration
  def change
    create_table :costs do |t|
      t.integer :post_id
      t.text :body
      t.integer :money

      t.timestamps null: false
    end
  end
end
