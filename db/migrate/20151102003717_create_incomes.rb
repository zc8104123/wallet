class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.integer :post_id
      t.text :body
      t.integer :money

      t.timestamps null: false
    end
  end
end
